package checkersGameModel;

/**
 * @author francoiseperrin
 *
 * Coordonnées des PieceModel
 */
public class Coord {

	private char colonne; 
	private int ligne;	
	private static final int MAX = ModelFactory.getlength();
	
	public Coord(char colonne, int ligne) {
		super();
		this.colonne = colonne;
		this.ligne = ligne;
	}
	public char getColonne() {
		return colonne;
	}
	public void setColonne(char colonne) {
		if (colonne >= 0 && colonne < Coord.MAX) {
			this.colonne = colonne;
		}
	}
	public int getLigne() {
		return ligne;
	}
	public void setLigne(int ligne) {
		if (ligne >= 0 && ligne < Coord.MAX) {
			this.ligne = ligne;
		}
	}
	

	@Override
	public String toString() {
		return "["+ligne + "," + colonne + "]";
	}
	

	public static void main(String[] args) {
		Coord c1 = new Coord('a', 7);
		Coord c2 = new Coord('b', 3);
		System.out.println("MAX = " + Coord.MAX);
		System.out.println("c1 = " + c1);
		System.out.println("c2 = " + c2);
			
	}
	
	
}
