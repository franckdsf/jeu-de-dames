package checkersGameModel;

import atelier1.checkersGameNutsAndBolts.PieceSquareColor;

public interface PieceModel {
	
	
	/**
	 * @return the coord
	 */
	public Coord getCoord() ;
	
	/**
	 * @param coord the coord to set
	 * le déplacement d'une pièce change ses coordonnées
	 */
	public void move(Coord targetCoord);


	/**
	 * @return the pieceColor
	 */
	public PieceSquareColor getPieceColor() ;
	
	
	/**
	 * @param targetCoord
	 * @param isPieceToTake
	 * @return true si le déplacement est légal
	 */
	public boolean isMoveOk(Coord targetCoord, boolean isPieceToTake);
	

}

