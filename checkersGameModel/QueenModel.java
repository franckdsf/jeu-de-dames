package checkersGameModel;
import atelier1.checkersGameNutsAndBolts.PieceSquareColor;

/**
 * @author francoiseperrin
 *
 *le mode de déplacement et de prise de la reine est différent de celui du pion
 */
public class QueenModel extends AbstractPieceModel{

    @Override
    public Coord getCoord() {
        return null;
    }

    @Override
    public void move(Coord targetCoord) {

    }

    @Override
    public PieceSquareColor getPieceColor() {
        return null;
    }

    @Override
    public boolean isMoveOk(Coord targetCoord, boolean isPieceToTake) {
        return false;
    }

}

