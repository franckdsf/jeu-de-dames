package checkersGameGui;

import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

import checkersGameController.Controller;
import javafx.scene.text.TextAlignment;

/**
 * @author francoise.perrin
 * 
 * Cette classe est la fenêtre du jeu de dames
 * Elle délègue a un objet la gestion de l'affichage du damier
 * 
 */
public class View extends GridPane {

	public View (Controller controller) {
		super();

		// le damier composé de carrés noirs et blancs
		// sur lesquels sont positionnés des pièces noires ou blanches
		Pane board = new Board(controller);

		// gestion de la taille du damier
		double height = GuiFactory.HEIGHT;			// TODO - à remplacer (atelier 4) : bad practice
		board.setPrefSize( height, height);			// TODO - à remplacer (atelier 4) : bad practice

		// création d'un fond d'écran qui contiendra le damier + les axes (atelier 2)
		BorderPane checkersBoard = new BorderPane();
		
		// ajout du damier au centre du fond d'écran
		checkersBoard.setCenter(board);

		// ajout des coords
		checkersBoard.setTop(createHorizontalCoord());
		checkersBoard.setBottom(createHorizontalCoord());
		checkersBoard.setLeft(createVerticalCoord());
		checkersBoard.setRight(createVerticalCoord());

		// ajout du fond d'écran à la vue
		this.add(checkersBoard, 0, 1);
	}

	private GridPane createHorizontalCoord(){
		GridPane gridpane = new GridPane();
		double size = GuiFactory.HEIGHT/GuiFactory.SIZE;
		String tab[] = {"a","b","c","d","e","f","g","h","i","j"};

		/* Crée un label vide de la taille
		de la colonne gauche */
		Label lab = new Label("");
		lab.setMinWidth(20);
		gridpane.add(lab,0,0);

		for(int i = 1; i <= 10; i ++){
			lab = new Label(tab[i-1]);
			lab.setMinWidth(size);
			lab.setAlignment(Pos.CENTER);
			gridpane.add(lab, i, 0);
		}

		return gridpane;
	}

	private GridPane createVerticalCoord(){
		GridPane gridpane = new GridPane();
		double size = GuiFactory.HEIGHT/GuiFactory.SIZE;

		Label lab;
		for(int i = 1; i <= 10; i ++){
			lab = new Label(Integer.toString(i));
			lab.setMinSize(20, size);
			lab.setAlignment(Pos.CENTER);
			gridpane.add(lab, 0, i);
		}

		return gridpane;
	}
}


